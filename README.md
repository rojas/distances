# distances

This is a python package for calculating various distances measures

# Installation
The project can be cloned locally using the command 

```shell
git clone git@gitlab.gwdg.de:rojas/distances.git
````

# Usage 
Currently the current measures distances are implemented

* Euclidean distance

# Testing
The package is tested with pytest. Run 
```shell
pytest test_euclidean.py
```

# License 
The package is licensed  under the terms of the [MIT License](https://opensource.org/licenses/MIT)